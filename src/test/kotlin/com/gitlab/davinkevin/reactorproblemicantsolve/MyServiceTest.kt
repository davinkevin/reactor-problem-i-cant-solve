package com.gitlab.davinkevin.reactorproblemicantsolve

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import reactor.core.publisher.toMono
import reactor.test.StepVerifier
import java.util.*

class MyServiceTest {

    private lateinit var lib: WebReactiveLib
    private lateinit var service: MyService

    @BeforeEach
    fun beforeEach() {
        lib = mock()
        service = MyService(lib)
    }

    @Nested
    @DisplayName("reactive")
    inner class Reactive {

        @Test
        fun `should do nothing because link between user and group already exist`() {
            /* Given */
            whenever(lib.findGroupsOfUser(any())).thenReturn(Groups(listOf(Group(UUID.randomUUID(), "label"))).toMono())

            /* When */
            StepVerifier.create(service.findGroupToCreateReactive(UUID.randomUUID(), "label"))
                    /* Then */
                    .expectSubscription()
                    .verifyComplete()
        }

        @Test
        fun `should trigger creation of the link between both user and group are not yet linked together`() {
            /* Given */
            val group = Group(UUID.randomUUID(), "label")
            val userId = UUID.randomUUID()
            whenever(lib.findGroupsOfUser(userId)).thenReturn(Groups(emptyList()).toMono())
            whenever(lib.searchGroups("label")).thenReturn(Groups(listOf(group)).toMono())

            /* When */
            StepVerifier.create(service.findGroupToCreateReactive(userId, group.label))
                    /* Then */
                    .expectSubscription()
                    .expectNext(LinkUserToGroup(userId, group.id))
                    .verifyComplete()
        }

        @Test
        fun `should throw error if required group is not associated to the user and not found in the search result`() {
            /* Given */
            val userId = UUID.randomUUID()
            whenever(lib.findGroupsOfUser(userId)).thenReturn(Groups(emptyList()).toMono())
            whenever(lib.searchGroups("label")).thenReturn(Groups(emptyList()).toMono())

            /* When */
            StepVerifier.create(service.findGroupToCreateReactive(userId, "label"))
                    /* Then */
                    .expectSubscription()
                    .expectError(IllegalStateException::class.java)
                    .verifyThenAssertThat()
                    .hasOperatorErrorWithMessage("Group label not found")
        }

    }

    @Nested
    @DisplayName("blocking")
    inner class Blocking {

        @Test
        fun `should do nothing because link between user and group already exist`() {
            /* Given */
            whenever(lib.findGroupsOfUser(any())).thenReturn(Groups(listOf(Group(UUID.randomUUID(), "label"))).toMono())

            /* When */
            val newLinkToCreate = service.findGroupToCreateBlocking(UUID.randomUUID(), "label")

            /* Then */
            assertThat(newLinkToCreate)
                    .isEmpty
        }

        @Test
        fun `should trigger creation of the link between both user and group are not yet linked together`() {
            /* Given */
            val group = Group(UUID.randomUUID(), "label")
            val userId = UUID.randomUUID()
            whenever(lib.findGroupsOfUser(userId)).thenReturn(Groups(emptyList()).toMono())
            whenever(lib.searchGroups("label")).thenReturn(Groups(listOf(group)).toMono())

            /* When */
            val newLink = service.findGroupToCreateBlocking(userId, group.label)

            /* Then */
            assertThat(newLink).isNotEmpty
            assertThat(newLink.get().userId).isEqualTo(userId)
            assertThat(newLink.get().groupId).isEqualTo(group.id)
        }

        @Test
        fun `should throw error if required group is not associated to the user and not found in the search result`() {
            /* Given */
            val userId = UUID.randomUUID()
            whenever(lib.findGroupsOfUser(userId)).thenReturn(Groups(emptyList()).toMono())
            whenever(lib.searchGroups("label")).thenReturn(Groups(emptyList()).toMono())

            /* When */
            assertThatThrownBy { service.findGroupToCreateBlocking(userId, "label") }
                    /* Then */
                    .isInstanceOf(IllegalStateException::class.java)
                    .hasMessage("Group label not found")
        }
    }



}
