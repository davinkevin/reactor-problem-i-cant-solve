package com.gitlab.davinkevin.reactorproblemicantsolve

import reactor.core.publisher.Mono
import reactor.core.publisher.toMono
import reactor.core.publisher.switchIfEmpty
import java.util.*

@Suppress("UNUSED_PARAMETER")
class WebReactiveLib {

    fun findGroupsOfUser(userId: UUID): Mono<Groups> = TODO("webclient call, not relevant...")
    fun searchGroups(label: String): Mono<Groups> = TODO("webclient call, not relevant...")

}

data class Groups(val items: List<Group>)
data class Group(val id: UUID, val label: String)
data class LinkUserToGroup(val userId: UUID, val groupId: UUID)

class MyService(private val lib: WebReactiveLib) {

    fun findGroupToCreateBlocking(userId: UUID, groupLabel: String): Optional<LinkUserToGroup> {
        val group = lib.findGroupsOfUser(userId)
                .flatMapIterable { it.items }
                .filter { it.label == groupLabel }
                .toMono()
                .blockOptional()

        if(group.isPresent) {
            return Optional.empty()
        }

        return lib.searchGroups(groupLabel)
                .flatMapIterable { it.items }
                .filter { it.label == groupLabel }
                .toMono()
                .map { LinkUserToGroup(userId, it.id) }
                .switchIfEmpty { IllegalStateException("Group $groupLabel not found").toMono() }
                .blockOptional()
    }

    fun findGroupToCreateReactive(userId: UUID, groupLabel: String): Mono<LinkUserToGroup> =
            lib.findGroupsOfUser(userId)
                    .flatMapIterable { it.items }
                    .filter { it.label == groupLabel }
                    .toMono()
                    .map { Optional.of(it) }
                    .defaultIfEmpty(Optional.empty())
                    .filter { g -> g.isEmpty }
                    .flatMap { lib.searchGroups(groupLabel)
                            .flatMapIterable { it.items }
                            .toMono()
                            .map { LinkUserToGroup(userId, it.id) }
                            .switchIfEmpty { IllegalStateException("Group $groupLabel not found").toMono() }
                    }

}
